#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


#include "GrafoSt2020.h"
#include "veinteveinte.h"
#include "otros.h"

Cola CrearCola(u32 size){
    struct Cola q;
    q.cabeza = q.cola = 0;
    q.fila = malloc(size*sizeof(u32));
    return q;
}
void DestruirCola(struct Cola *q){
    free(q->fila);
    q->fila = NULL;
}
void Encolar(struct Cola *q,u32 nodo){
    q->fila[q->cola]=nodo;
    q->cola++;
}
u32 Decolar(struct Cola *q){
    u32 nodo=0;
    nodo=q->fila[q->cabeza];
    q->cabeza++;
    return nodo;
}
u32 Vacia(struct Cola *q){
    return (q->cabeza==q->cola);
}