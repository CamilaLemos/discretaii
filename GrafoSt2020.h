#ifndef __GRAFOST2020_H
#define __GRAFOST2020_H

#include <stdio.h>
#include <stdlib.h>


/*Se utilizará el tipo de dato u32 para especificar
un entero de 32 bits sin signo.Todos los enteros
sin signo de 32 bits deberán usar este tipo de dato.*/
typedef unsigned int u32;


/*Info_vert contiene nombre real, inicio para
  definir donde comienzan sus vecinos en el areglo,
  grado(cantidad de vecinos), y su color en ese momento.*/
typedef struct Info_vert {
	u32 nreal;
	u32 nfict;
	u32 inicio;
	u32 grado;
	u32 color;
  u32 orden;
}Info_vert;

/*GrafoSt contendrá:
	-num de vertices
	-num de lados
	-Delta del grafo (el mayor grado)
	-cosas de cada vertice:
		-->nombre de c/vertice
		-->grados de c/vertice (cant de vecinos) 
		-->colores de c/vertice
  	-orden de vertices
  	-orden natural de vertices. 
  	-vecinos para c/vertice
*/
typedef struct GrafoSt {
    u32 cant_vert;
    u32 cant_lados;
    u32 delta;
    u32 colores;
    Info_vert *vertices;
    u32 *ordenNat; 
    u32 *vecinos;
    u32 *orden;
}GrafoSt;


#endif