#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


#include "GrafoSt2020.h"
#include "veinteveinte.h"
#include "otros.h"


/*Función para poder ordenar de manera decreciente
  el segundo elemento de cada Dupla*/
int compareV2(const void *a, const void *b){
    u32 x = ((struct Dupla*)a)->v2; 
    u32 y = ((struct Dupla*)b)->v2; 
    if(x <= y){
        return 1;
    }else{
        return -1;
    }   
    return 0;
}
int compareCrecienteV2(const void *a, const void *b){
    u32 x = ((struct Dupla *)a)->v2; 
    u32 y = ((struct Dupla *)b)->v2; 
    if (x < y) return -1;
    else if (x > y) return 1;
    return 0; 
}

/*Función para poder ordenar de manera creciente
  el segundo elemento de cada 3-upla*/
int compareColorCG(const void *a, const void *b){
    u32 x = ((struct Trupla *)a)->v2; 
    u32 y = ((struct Trupla *)b)->v2; 
    if (x < y) return -1;
    else if (x > y) return 1;
    return 0;  
}
/*Función para poder ordenar de manera creciente
  el tercer elemento de cada 3-upla*/
int compareCGBC(const void *a, const void *b){
    u32 x = ((struct Trupla *)a)->v3; 
    u32 y = ((struct Trupla *)b)->v3; 
    if (x < y) return -1;
    else if (x > y) return 1;
    return 0; 
}
//La utilizo en ChicoGrandeCGBC
char frecuenciaColores(Trupla *b, u32 n){
    u32 color = 0;
    u32 j = 0;
    u32 coloreados = 0;
    u32 cant_coloreados = 0;
    for(u32 i = 0; i < n; i++){
        if(b[i].v2 == color){
            coloreados++;
        }else{
            i = i-1;
            j = i;
            cant_coloreados = coloreados;
            while(coloreados != 0){
                b[j].v3 = cant_coloreados;
                coloreados--;
                j--;
            }
            color++;
        }
    }
    return 0;
}
void swap(u32* a, u32* b){
    u32 temp = *a;
    *a = *b;
    *b = temp;
}
void OrdenNat(Grafo g){
    u32 n = NumeroDeVertices(g);
    for(u32 i = 0; i < n; i++){
        FijarOrden(i, g, i);
    }
}
