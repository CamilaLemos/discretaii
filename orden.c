#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "GrafoSt2020.h"
#include "veinteveinte.h"
#include "otros.h"



/*Ordena de acuerdo con el orden Welsh-Powell, es decir,
con los grados en orden no creciente.Por ejemplo
si hacemos un for en i de Grado(i,G) luego de haber corrido
WelshPowell(G), deberiamos obtener algo como:
10,9,7,7,7,7,5,4,4.
Retorna 0 si todo anduvo bien, 1 si hubo algún problema.*/
char WelshPowell(Grafo G){
    u32 n = NumeroDeVertices(G);
    Dupla *b = NULL;
    b = calloc(n, sizeof(Dupla));
    if (b == NULL)
        return 1;
    OrdenNat(G);
    for(u32 i = 0; i < n; i++){
        b[i].v1 = i;
        b[i].v2 = Grado(i, G);
    }
    qsort(b,n, sizeof(Dupla), compareV2);
    for(u32 i = 0; i < n; i++){
        FijarOrden(i, G, b[i].v1);
    }
    free(b);
    b = NULL;
    return 0;
}

/*Revierte los bloques de colores: Si G esta coloreado con r colores 
y V(0) son los vertices coloreados con 0, V(1) los vertices
coloreados con 1, V(2) los coloreados con 2, etc
entonces esta función ordena los vertices poniendo primero los vertices
de V(r−1) ,luego los de V(r−2) , luego los de V(r−3) , etc, hasta V(0).
 En otras palabras, para todo i = 0, .., r − 2 los vertices de V(i)
estan todos, luego de los vertices de V(i+1). (el orden entre 
los vértices de cada V(i) no se especifica).
Retorna 0 si todo anduvo bien, 1 si hubo algún problema.*/
char RevierteBC(Grafo G){
    u32 n = NumeroDeVertices(G);
    Dupla *b = NULL;
    b = calloc(n, sizeof(Dupla));
    if(b == NULL)
        return 1;
    OrdenNat(G);
    for(u32 i = 0; i < n; i++){
        b[i].v1 = i;
        b[i].v2 = Color(i, G);
    }
    qsort(b, n, sizeof(Dupla), compareV2);

    for(u32 i = 0; i < n; i++){
        FijarOrden(i, G, b[i].v1);
    }
    free(b);
    b = NULL;
    return 0;

}


/*Ordena por cardinalidad de los bloques de colores, de menor a mayor:
Si G esta coloreado con r colores y V(0) son los vertices coloreados con 0,
V(1) los vertices coloreados con 1, etc, entonces
esta función ordena los vertices poniendo primero los vertices de V(j1),
luego los de V(j2), etc, donde j1, j2, ..., jr son tales que
|V (j1) | ≤ |V (j2) | ≤ ... ≤ |V (jr) |. 
(el orden entre los vértices de cada V(i) no se especifica).
Retorna 0 si todo anduvo bien, 1 si hubo algún problema.*/
char ChicoGrandeBC(Grafo G){
    u32 n = NumeroDeVertices(G);
    Trupla *b = NULL;
    b = calloc(n, sizeof(Trupla));
    if(b == NULL)
        return 1;
    OrdenNat(G);
    //ordeno por color
    for(u32 i = 0; i < n; i++){
        b[i].v1 = i;
        b[i].v2 = Color(i, G);
        b[i].v3 = 0;
    }
    qsort(b, n, sizeof(Trupla), compareColorCG);
    frecuenciaColores(b, n);
    qsort(b, n, sizeof(Trupla), compareCGBC);
    for(u32 i = 0; i < n; i++)
        FijarOrden(i, G, b[i].v1);
 
    free(b);
    b = NULL;
    return 0;
}

/*“Aleatoriza” el orden de los vértices de G, 
usando como semilla de aleatoridad el número R.
El orden no será aleatorio sino “pseudoaleatorio”
y debe depender determinı́sticamente de la variable R.
(es decir, correr dos veces esta función con R=4 pej, 
debe dar los mismos resultados, pero si R=12, 
debe dar otro resultado, el cual deberı́a ser 
sustancialmente distinto del anterior).
IMPORTANTE: debe dar el mismo resultado independientemente
del coloreo o del orden que tenga en ese momento G. 
Es decir, si esta función es llamada en dos lugares distintos
de un programa con la misma semilla, el orden obtenido debe ser
el mismo.
Debe cambiar el orden de los vértices PERO NO EL COLOR 
de los vertices. Idealmente la función debe ser tal que 
para cualquier orden posible de los vértices exista algún R
tal que AleatorizarVertices(G,R) obtenga ese orden: 
No les pido tanto, pero si les pido que no haya ninguna
restricción obvia en los ordenes que se puedan obtener. 
Pej, una función que, con vértices 4,5,7,10,15 sólo es capaz 
de producir los ordenes (4,5,7,10,15);(5,7,10,15,4);
(7,10,15,4,5);(10,15,4,5,7) y (15,4,5,7,10) NO ES aceptable.
Una función que, pej, en el 90% de los casos hace
que el primer vértice sea 4 tampoco es aceptable.
Retorna 0 si todo anduvo bien y 1 si hubo algún problema.
*/
char AleatorizarVertices(Grafo g, u32 R){
    char result = 0;
    u32 rand_index;
    u32 n = NumeroDeVertices(g);
    u32 *vertices = calloc(n, sizeof(u32));
    // El orden que tiene g es el natural
    OrdenNat(g);
    /* Almacenar los nuevos indices de los vertices, 
    la posicion indica el Orden y el valor el Indice en OrdenNatural*/
    if(vertices == NULL)
        return 1;
    for(u32 i = 0; i < n; i++)
        vertices[i] = i;
    // R dada por el usuario para rand()
    srand(R);
    for(u32 i = n - 1; i > 0; i--){
        /* i es indice valido, luego i+1 puede no serlo. 
        si i = n - 1 -> i + 1 = n, n no es indice valido por eso % (i+1)*/
        rand_index = (u32) rand() % (i + 1);
        // Hacer un swap de los elementos en indice i y rand_index
        swap(&(vertices[i]), &(vertices[rand_index]));
    }
    for(u32 i = 0; i < n && !result; i++){
        result = FijarOrden(i, g, vertices[i]);
    }
    free(vertices);
    return result;
}

/*Función de cálculo de num de componentes conexas.*/
u32 NumCCs(Grafo G){
    u32 n = NumeroDeVertices(G);
    u32 grado_vertice = 0;
    u32 vertice = 0;
    u32 indice_vecino = 0;
    u32 num_cc = 0;
    struct Cola q;
    bool visitados[n];

    memset(visitados,false,n*sizeof(bool));
    q = CrearCola(n);

    for(u32 iter = 0; iter < n  ; iter++){
        if(visitados[iter])
            continue;
        visitados[iter] = true;
        num_cc++;
        Encolar(&q, iter);

        while(!Vacia(&q)){
            vertice = Decolar(&q);
            grado_vertice = Grado(vertice, G);

            for(u32 vecino = 0; vecino < grado_vertice; vecino++){
                indice_vecino = OrdenVecino(vecino, vertice, G);
                if(visitados[indice_vecino] == false){
                    visitados[indice_vecino] = true;
                    // Colocar el vertice en la cola
                    Encolar(&q, indice_vecino);
                }
            }
        }
    }
    DestruirCola(&q);
    return num_cc;
}