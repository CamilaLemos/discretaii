#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "GrafoSt2020.h"
#include "veinteveinte.h"
#include "otros.h"



void DestruccionDelGrafo(Grafo G){
    free(G->vertices);
    free(G->ordenNat);
    free(G->orden);
    free(G->vecinos);
    free(G);
    G=NULL;
}
/*Incializa la estructura del grafo*/
Grafo CrearGrafo(Grafo g,u32 n,u32 m){
    g = (Grafo)malloc(sizeof(GrafoSt));
    g->vertices = calloc(n,sizeof(Info_vert));
    g->ordenNat = calloc(n,sizeof(u32));
    g->orden = calloc(n,sizeof(u32));
    g->vecinos = calloc(2*m,sizeof(u32));
    g->cant_vert = n;
    g->cant_lados = m;
    g->delta = 0;
    g->colores = 0;
    return g;
}
/* Ordena duplas de manera ascendente */
int compare1(const void *a, const void *b) {
    Dupla x=*(const Dupla *)a;
    Dupla y=*(const Dupla *)b;
    if(x.v1 <= y.v1){
        if(x.v1 == y.v1 && x.v2 > y.v2)
                return -1;  
        return 1;
    }else{
        return -1;
    }   
    return 0;
}

/*Función para poder ordenar de manera decreciente
  el segundo elemento de cada Dupla*/
int compare_decrec(const void *a, const void *b){
    u32 x= ((struct Dupla *)a)->v2; 
    u32 y= ((struct Dupla *)b)->v2; 
    if(x <= y){
        return 1;
    }else{
        return -1;
    }   
    return 0;
}

/*Función para poder ordenar de manera creciente
  el primer elemento de cada Dupla*/
int compare_crec(const void *a, const void *b){
    u32 x = ((struct Dupla *)a)->v1; 
    u32 y = ((struct Dupla *)b)->v1; 
    if (x < y) return -1;
    else if (x > y) return 1;
    return 0;  
}
int comp2(const void *a, const void *b){
    u32 x = ((struct Dupla *)a)->v2; 
    u32 y = ((struct Dupla *)b)->v2; 
    if (x < y) return -1;
    else if (x > y) return 1;
    return 0;  
}

char OrdenNatural(Grafo G){ 
    u32 n=G->cant_vert;
    Dupla *b=NULL;
    b=calloc(n,sizeof(Dupla));
    if(b!=NULL){
        for(u32 i=0; i < n; i++){
            b[i].v1=i;
            b[i].v2=G->vertices[i].nreal;
        }
        qsort(b,n, sizeof(Dupla),comp2);
        for(u32 i=0; i < n; i++){
            G->ordenNat[i]=b[i].v1;
        }
        free(b);
        return 0;
    }else{
        return 1;
    }
}
/*Es la función que realmente construye el 
  grafo a través de los datos tomados de un archivo*/ 
Grafo ArmoGrafo(Grafo g,Dupla *array){
    u32 iter=0;
    Dupla *b=NULL;
    u32 m=g->cant_lados;
    u32 gr=0;
    iter=0;
    u32 comparador=0xffffffff;
    u32 vertice=0;
    u32 vecino=0;
    g->delta = 1; 
    b=calloc(2*m,sizeof(Dupla));
    if(b!=NULL){
        /*b será un arreglo de duplas donde tendremos al
        arreglo "a" al comienzo,siguiendo de "a" invertido:
        a= [(v1,v2),(v3,v4),(v5,v6)]
        b= [(v1,v2),(v3,v4),(v5,v6),(v2,v1),(v4,v3),(v5,v6)]
        */
        for (u32 i=0; i < 2*m ; i++){
            if(i <= m-1){
                b[i]=array[iter];
                iter++;
                if(i==m-1)
                    iter=0;
            }else{
                b[i].v1=array[iter].v2;
                b[i].v2=array[iter].v1;
                iter++;
            }
        }
        //ordeno b de manera ascendente.
        qsort(b,2*m,sizeof(Dupla),compare1);
        iter=0;
        for(u32 i=0; i < 2*m; i++){
            vertice=b[i].v1;
            if(comparador==vertice){
                gr++;
                if(i==2*m-1)
                    g->vertices[iter-1].grado=gr;
            }else{
                if(iter==0){
                    g->vertices[iter].inicio=0;
                }else{
                    g->vertices[iter-1].grado=gr;
                    g->vertices[iter].inicio=g->vertices[iter-1].inicio+gr;
                }
                gr=1;
                g->vertices[iter].grado=gr;
                comparador=vertice;
                g->vertices[iter].nreal=vertice;
                g->vertices[iter].nfict=iter;
                g->orden[iter]=iter;

                g->vertices[iter].orden = iter;

                g->vertices[iter].color=0;
                iter++;
            }
        }
        g->cant_vert=iter;
        /*El arreglo de vecinos, tendrá los vecinos de todos
          los vertices, pero con sus nombres ficticios.*/

        /*cambio los datos del arreglo b en la segunda posicion 
          de cada dupla por su nombre ficticio.*/

        /*en 1er elemento de las duplas guardo el orden con el 
        que tiene actualmente.*/
        
        for(u32 i=0; i < 2*m; i++)
            b[i].v1=i;

        //ordeno por segundo elementos de manera decreciente
        qsort(b,2*m, sizeof(Dupla), compare_decrec);
        iter=0;
        //busco nombre ficticio
        for(u32 i=0; i < 2*m ; i++){
            vecino=b[i].v2;
            while(vecino != g->vertices[iter].nreal && iter < g->cant_vert)
                iter++;
            if(g->vertices[iter].nreal==vecino)
                b[i].v2=g->vertices[iter].nfict;
        }
        //vuelvo al orden anterior
        qsort(b,2*m, sizeof(Dupla), compare_crec);
        //guardo en g->vecinos los datos que hay en b[i].v2
        for(u32 i=0;i < 2*m;i++){
            g->vecinos[i]=b[i].v2;
        }
        free(b);
        for(u32 i=0;i < g->cant_vert; i++){
            if(i < g->cant_vert-1)
                g->delta = MAX(g->delta, MAX(g->vertices[i].grado,g->vertices[i+1].grado));
        }
        OrdenNatural(g);
    }
    return g;
}
/*La función aloca memoria, inicializa lo que haya que inicializar 
de una estructura GrafoSt ,lee un grafo desde standard
input, lo carga en la estructura, incluyendo algún orden 
de los vertices, corre Greedy con ese orden para dejar todos
los vertices coloreados y devuelve un puntero a la estructura.
En caso de error, la función devolverá un puntero a NULL.
(errores posibles pueden ser falla en alocar memoria, pero
también que el formato de entrada no sea válido. Por ejemplo,
una cierta linea se indicará un número m que indica cuantos
lados habrá y a continuación debe haber m lineas cada una 
de las cuales indica un lado. Si no hay AL MENOS m lineas 
luego de esa, debe retornar NULL.*/

Grafo ConstruccionDelGrafo(){
    Grafo g=NULL;
    Dupla *a;
    u32 n,m,l;
    bool follow = true;
    char input;
    char edge[4];
    u32 v1,v2,i;
    int ret;
    l=0;
    i=0;
    while(follow && !feof(stdin)){
        input=(char)fgetc(stdin);
        if(input =='c'){
            //mientras no termine la linea o no termine el archivo,leo cada caracter.
            while(getchar() != '\n' && !feof(stdin));
            //si el archivo termina:
            if(feof(stdin)){
                printf("no hay lineas despues del comentario\n");
                return g;
            }
            //volver a entrar al while
            continue;
        }else if(input == 'p'){
            //DEBE EMPEZAR CON P EDGE,GUARDO N y M
            /*esta verificación de la función scanf(ret) es para
            que no haya warnings por no haber utilizado el
            valor de salida de cada scanf */
            ret=scanf(" %s",edge);
            // fgets(edge, sizeof(edge), stdin);
            // Si ret es cero en la comparacion de edge, entonces vamos bien
            ret=strcmp("edge\0",edge);
            if(ret != 0) {
                printf("error en primera linea sin comentario\n");
                return g;
            }else{
                // si va todo bien, escaneo n=cant vert, m=cant lados.
                ret=scanf("%u %u",&n,&m);
                //terminamos este while poniendo el candado follow en false.
                follow = false;
                //mientras no termine la linea o no termine el archivo,leo cada caracter.
                while(getchar() != '\n' && !feof(stdin));
            }
        }else{
            //despues del comentario, si no viene un p edge, debe printear esto.
            printf("error en primera linea sin comentario\n");
            return g;
        }
    }
    g=CrearGrafo(g,n,m);
    //verifico que se haya podido alocar toda esa memoria.
    if(g!=NULL && g->vertices!=NULL && g->ordenNat!=NULL && g->vecinos!=NULL && g->orden!=NULL){
        /*el arreglo a es una copia de los lados leidos.
          será un arreglo de duplas  */
        a=calloc(m,sizeof(Dupla));
        if(a!=NULL){
            //l cantidad de lados que voy scaneando.
            //mientras l no pase cant de lados y no termine el archivo,scaneo.
            while(l<m && !feof(stdin)){
                l=l+1;
                ret=scanf(" %c",&input);
                if(input =='e'){
                    ret=scanf(" %u %u",&v1,&v2);
                    a[i].v1=v1;
                    a[i].v2=v2;
                    i++;
                    // Si no es la ultima linea,leo c/caracter que sobra.
                    if(!(feof(stdin)))
                        while(getchar() != '\n');
                }else{
                    //si en alguna linea no comienza con e, libero el grafo
                    // y tiro mensaje de error.
                    free(a);
                    DestruccionDelGrafo(g);
                    printf("error de lectura en lado %u \n",l);
                    return g;
                }
            }
            //debemos tener al menos M lados.
            if(l < m){
                //le sumo uno por que para saber q faltan lados,
                // aviso que hay error en la linea q no hay nada.
                l=l+1;
                printf("error en la linea %u",l);
                DestruccionDelGrafo(g);
                free(a);
                return g;
            }
            /*la función ArmoGrafo hará la verdadera construcción
            con la estructura nuestra.*/
            g=ArmoGrafo(g,a);
            free(a);
        }
        //Verifico que haya exactamente misma cantidad de vertices.
        if(n!=g->cant_vert){
            DestruccionDelGrafo(g);
            return g;
        }
        //coloreo con greedy.
        g->colores=Greedy(g);
    }
    return g;

}

/*La función aloca memoria suficiente para copiar 
todos los datos guardados en G, hace una copia de
G en esa memoria y devuelve un puntero a esa memoria.
En caso de no poder alocarse suficiente memoria, 
la función devolverá un puntero a NULL.
Esta función se puede usar (y la usaremos asi en 
un main) para realizar una o mas copias de G, 
intentar diversas estrategias de coloreo por cada copia,
y quedarse con la que de el mejor coloreo.*/
Grafo CopiarGrafo(Grafo G){
    Grafo f = NULL;
    if(G != NULL){
        u32 n = G->cant_vert;
        u32 m = G->cant_lados;
        f = CrearGrafo(f,n,m); 
        if(f!=NULL && f->orden!=NULL && f->vertices!=NULL && f->vecinos!=NULL){
            memcpy(f->vertices, G->vertices, n*sizeof(struct Info_vert));
            memcpy(f->orden, G->orden,n*sizeof(u32));
            memcpy(f->ordenNat, G->ordenNat,n*sizeof(u32));
            memcpy(f->vecinos, G->vecinos,2*m*sizeof(u32));
            f->delta = G->delta;
            f->cant_vert = G->cant_vert;
            f->cant_lados = G->cant_lados;
        }
    }
    return f;
}
/*---- tienen que ser O(1)---*/
u32 NumeroDeVertices(Grafo G){
    u32 result = 0;
    if(G != NULL)
        result = G->cant_vert;
    return result;
}

u32 NumeroDeLados(Grafo G){
	u32 result=0;
    if(G != NULL)
        result = G->cant_lados;
    return result;
}

//mayor grado de G
u32 Delta(Grafo G){
	u32 result = 0;
    if(G != NULL)
        result = G->delta;
    return result;
}

//nombre real en la posicion i del orden actual
u32 Nombre(u32 i,Grafo G){
    u32 result = 0;
    if(G != NULL)
            result = G->vertices[G->orden[i]].nreal;
    return result;
}


/*Devuelve el color con el que está coloreado el vértice número i 
en el orden guardado en ese momento en G.Si i es mayor o igual que 
el número de vértices, devuelve 2^(32−1).(esto nunca puede ser un color
en los grafos que testeeemos, pues para que eso fuese un color de algún 
vértice, el grafo deberia tener al menos 2^32 vertices, lo cual lo hace 
inmanejable).
*/
u32 Color(u32 i,Grafo G){
    u32 result = 0xffffffff;
    if(G == NULL || i >= G->cant_vert)
        return result;
    result = G->vertices[G->orden[i]].color;
    return result;
}
/*Devuelve el grado del vértice número i en el orden guardado
en ese momento en G. Si i es mayor o igual que el número de vértices,
devuelve 2^ (32−1). (esto nunca puede ser un grado en los grafos que
testeeemos, pues para que eso fuese un grado de algún vértice, 
el grafo deberia tener al menos 2^32 vertices, lo cual lo hace
inmanejable).*/
u32 Grado(u32 i,Grafo G){
    u32 result = 0xffffffff;
    if(G == NULL || i >= G->cant_vert)
        return result;
    result = G->vertices[G->orden[i]].grado;
    return result;
}

u32 ColorVecino(u32 j,u32 i,Grafo G){
    u32 result=0xffffffff;
    u32 jvecino=0;
    if(G==NULL || i >= G->cant_vert || j >= G->vertices[G->orden[i]].grado)
        return result;
    jvecino=G->vertices[G->orden[i]].inicio+j;
    result=G->vertices[G->vecinos[jvecino]].color;
    return result;
}

u32 NombreVecino(u32 j,u32 i,Grafo G){
    u32 result=0xffffffff;
    u32 jvecino=0;
    if(G == NULL || i >= G->cant_vert || j >= G->vertices[G->orden[i]].grado)
        return result;
    jvecino=G->vertices[G->orden[i]].inicio+j;
    result=G->vertices[G->vecinos[jvecino]].nreal;
    return result;
}
/*OrdenVecino(j,i,Grafo G) es igual a k si y sólo si 
el vécino j-ésimo del i-ésimo vértice de G en el orden interno 
es el k-ésimo vértice del orden interno de G.*/
u32 OrdenVecino(u32 j,u32 i,Grafo G){
    u32 k = 0xffffffff; 
    u32 jvecino = 0;  
    if(G == NULL || i >= G->cant_vert || j >= G->vertices[G->orden[i]].grado)
        return k;
    jvecino=G->vertices[G->orden[i]].inicio+j;
    k=G->vertices[G->vecinos[jvecino]].orden;
    return k;
}
/*Si i es menor que el número de vértices, 
le asigna el color x al vértice número i 
en el orden guardado en ese momento en
G y retorna 0.
De lo contrario, retorna 1.
Esta función debe ser usada con cuidado pues 
puede provocar que queden colores no asignados a 
ningún vértice (pej,dando un coloreo con colores 0,1,4,7)*/
char FijarColor(u32 x,u32 i,Grafo G){
    if( i < G->cant_vert){
        G->vertices[G->orden[i]].color = x;
        return 0;
    }
    return 1;
}

/*Si i y N son menores que el número de vértices, 
fija que el vértice i en el orden guardado en G 
será el N -ésimo vértice del orden “natural”
(el que se obtiene al ordenar los vértices en orden
creciente de sus “nombres” reales) y retorna 0. De lo
contrario retorna 1.*/
char FijarOrden(u32 i,Grafo G,u32 N){
    if( i < G->cant_vert && N < G->cant_vert ){
        G->orden[i] = G->ordenNat[N];
        G->vertices[G->ordenNat[N]].orden = i ; 
        return 0;
    }
    return 1;
}
