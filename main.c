#include "veinteveinte.h"
#include "time.h"


//gcc -Wall -Werror -Wextra -Wpedantic -std=c99 -Wstrict-prototypes -Wunreachable-code -Wconversion -Wshadow -O3 -g -c cola.c aux.c coloreo.c orden.c otros.c && gcc -Wall -Werror -Wextra -Wpedantic -std=c99 -Wstrict-prototypes -Wunreachable-code -Wconversion -Wshadow -O3 -g -o out *.o main.c && ./out < ./input/ej1.in 

/*
static void print_help(char *program_name) {
    printf("Usage: %s <input file path> [ OPTIONS ]\n\n", program_name);
}

static char *parse_filepath(int argc, char *argv[]) {
    if (argc < 2) {
        print_help(argv[0]);
        exit(EXIT_FAILURE);
    }
    return argv[1];
}*/

static void ImprimirGradoColor(Grafo G, char selector) {
    for (u32 i = 0; i < NumeroDeVertices(G); i++) {
        printf("%u ", selector ? Grado(i, G): Color(i,G));
    }
    printf("\n");
}

static void ImprimirGrafo(Grafo G, int impG) {
    for (u32 i = 0; i < NumeroDeVertices(G) && impG; i++) {
        printf("NR: %u  NF= %u GR= %u C= %u VEC="
              , Nombre(i, G), i, Grado(i,G), Color(i, G));
        for (u32 j = 0; j < Grado(i, G); j++) {
            printf("  %u", NombreVecino(j, i, G));
        }
        printf("\n");
    }
}

static void ImprimirVecinos(Grafo G, int impVc) {
    for (u32 i = 0; i < NumeroDeVertices(G) && impVc; i++) {
        for (u32 j = 0; j < Grado(i, G); j++) {
            printf("  %u, ", NombreVecino(j, i, G));
        }
    }
        printf("\n");
}

void test_AleatorizarVertices(Grafo g, u32 seed_start, u32 seed_end){
	printf("Test de AleatorizarVertices:\n**Grafo original**\n");
	ImprimirGrafo(g, 1);
	char exito = 0;
	for(;seed_start <= seed_end && exito == 0; seed_start++){
		exito = AleatorizarVertices(g, seed_start);
		printf("AleatorizarVertices con seed=%u:\n", seed_start);
		ImprimirGrafo(g, 1);
	}
}

static void Testear(Grafo G, int WP, int BC, int CGBC, int IMPT, int AV) {
    double start, end;
    printf("\nNúmero de vértices: %u\n", NumeroDeVertices(G));
    printf("Número de lados: %u\n", NumeroDeLados(G));
    printf("Delta del grafo: %u\n", Delta(G));
    if (WP) {
        if (IMPT) {printf("Antes WP:\n"); ImprimirGradoColor(G, 1);}
        start = (double)clock();
        WelshPowell(G);
        end = (double)clock();
        printf("Despues WP: %f\n", (end-start)/CLOCKS_PER_SEC);
        if (IMPT) {ImprimirGradoColor(G, 1);}
        WelshPowell(G);
        printf(" Despues de wp2 :\n");
        if (IMPT) {ImprimirGradoColor(G, 1);}
    }
    if (BC) {
        if (IMPT) {printf("Antes BC:\n"); ImprimirGradoColor(G, 0);}
        start = (double)clock();
        RevierteBC(G);
        end = (double)clock();
        printf("Despues BC: %f\n", (end-start)/CLOCKS_PER_SEC);
        if (IMPT) {ImprimirGradoColor(G, 0);}
        RevierteBC(G);
        printf("Despues BC 2:\n");
        if (IMPT) {ImprimirGradoColor(G, 0);}
    }
    if (CGBC) {
        if (IMPT) {printf("Antes CGBC:\n"); ImprimirGradoColor(G, 0);}
        start = (double)clock();
        ChicoGrandeBC(G);
        end = (double)clock();
        printf("Despues CGBC: %f\n", (end-start)/CLOCKS_PER_SEC);
        if (IMPT) {ImprimirGradoColor(G, 0);}
        ChicoGrandeBC(G);
        end = (double)clock();
        printf("Despues CGBC 2: \n");
        if (IMPT) {ImprimirGradoColor(G, 0);}
    }
	if(AV){
		// Los ultimos 2 son rangos de R para la funcion
		test_AleatorizarVertices(G, 1, 10);
	}
}
static void TestearC(Grafo G, int SC, int GR, int B, int IMPT) {
    double start, end;
    char r;
    if(SC){
        ImprimirGradoColor(G, 0);
        SwitchColores(G, 2, 0);
        ImprimirGradoColor(G, 0);
    }
    if (GR){
        printf(" GREDY\n");
        printf("Colores usados: %u \n",Greedy(G));
        if (IMPT) { ImprimirGrafo(G, 1);}

    }
    if (B){
        start = (double)clock();
        r = Bipartito(G);
        end = (double)clock();
        printf("Time Bipartito: %f\n", (end-start)/CLOCKS_PER_SEC);
        if (r){ printf(" Es Bipartito \n");
        }else { printf(" No es Bipartito \n");}
    }

}

int main(void) {
    //char *filepath = parse_filepath(argc, argv);
    //FILE *file = fopen(, "r");
    Grafo g;
    double start, end;
    int IMPVC = 0;
    int IMPG = 1;
    int IMPT = 1;
    int COP = 0;
    int WP = 0;
    int BC = 0;
    int CGBC = 0;
    int SC = 0;
    int GR = 0;
    int B = 1;
	// AleatorizarVertices
	int AV = 0;
    start = (double)clock();
    g = ConstruccionDelGrafo();
    end = (double)clock();
    ImprimirGrafo(g, IMPG);
    ImprimirVecinos(g, IMPVC);
    printf("\nTiempo de carga del grafo: %f", (end-start)/CLOCKS_PER_SEC);
    Testear(g, WP, BC, CGBC, IMPT, AV);
    TestearC(g, SC, GR, B, IMPT);
    if (COP) {
        printf("\n-------------------------------------------------\n");
        start = (double)clock();
        Grafo H = CopiarGrafo(g);
        end = (double)clock();
        //ImprimirGrafo(H, IMPG);
        printf("\nTiempo de copiado del grafo: %f\n\n", (end-start)/CLOCKS_PER_SEC);
        Testear(H, WP, BC, CGBC, IMPT, AV);
        DestruccionDelGrafo(H);
    }

    DestruccionDelGrafo(g);
    //fclose(file);
    return EXIT_SUCCESS;
}