CC = gcc
CFLAGS = -Wall -Wextra -Werror -pedantic -std=c99 -O3
SRC = $(wildcard *.c)
OBJ = $(SRC:.c=.o)

# NOMBRE DEL ARCHIVO COMPILADO
TARGET = discreta2

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $^

.PHONY: clean
clean:
	rm *.o $(TARGET)
