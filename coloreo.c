#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "GrafoSt2020.h"
#include "veinteveinte.h"
#include "otros.h"

/*Greedy en G comienza coloreando con el color 0, 
con el orden interno. Devuelve el número de colores
que se obtiene. Si su implementación de Greedy usa algún alloc 
debe devolver 2 32 − 1 si el alloc falla. (este número es demasiado grande
para que pueda ser la cantidad de colores de cualquiera de los grafos que usaremos).*/
u32 Greedy(Grafo G){
    u32 n = NumeroDeVertices(G);
    u32 u_grado = 0;
    u32 color = 0;
    bool ocupados[n];
    u32 mcolor = 0;
    //seteo todos los colores en false
    memset(ocupados,false,n*sizeof(bool));
    //dejo sin colores a los vertices.
    for(u32 i = 0; i < n; i++)
        FijarColor(0xffffffff,i ,G);

    //pongo primer vertice con el primer color
    FijarColor(0,0,G);
    
    for (u32 u = 0; u < n; u++) {
        u_grado = Grado(u,G);
         /*Revisamos los vecinos del vertice u.
          y marcamos los colores ocupados. */  
        for (u32 i = 0; i < u_grado; i++){
            color = ColorVecino(i,u,G);
            if(color != 0xffffffff)
                ocupados[color] = true;
        }
        //Buscamos el primer color disponible 
        for (u32 j = 0; j < n; j++){
            if (!ocupados[j]){
                FijarColor(j,u,G);
                //necesito guardar cuantos colores voy usando
                mcolor = MAX(mcolor, j);
                break;
            }
        }
        // Reseteamos el array de colores de los vecinos
       for (u32 i = 0; i < u_grado; i++) {
            color = ColorVecino(i,u,G);
            if(color != 0xffffffff)
                ocupados[color] = false;
        }
    }
    //retorno la cant de colores que use
    //como empieza en 0, le sumo uno.
    return mcolor+1;
}

/*Devuelve 1 si G es bipartito, 0 si no. Si devuelve
1, colorea G con un coloreo propio de dos colores. 
Si devuelve 0, debe dejar a G coloreado con algún
coloreo propio.*/
char Bipartito(Grafo G){
    char es_bipartito = (char) 1;
    u32 n = NumeroDeVertices(G);
    Cola q;
    u32 color_vecino, indice_vecino;
    u32 vertice, grado_vertice, color_vertice;

    //Dejo a cada nodo sin color.
    for(u32 i = 0; i < n; i++)
        FijarColor(0xffffffff, i, G);

    q = CrearCola(n);


    for(u32 iter = 0; iter < n && es_bipartito; iter++){
        color_vertice = Color(iter, G);
        if(color_vertice != 0xffffffff)
            continue;

        FijarColor(0, iter, G);
        Encolar(&q, iter);

        while(!Vacia(&q) && es_bipartito){
            vertice = Decolar(&q);
            grado_vertice = Grado(vertice, G);
            color_vertice = Color(vertice, G);

            for(u32 vecino = 0; vecino < grado_vertice; vecino++){
                color_vecino = ColorVecino(vecino, vertice, G);
                if(color_vecino == 0xffffffff){
                    color_vecino = 1 - color_vertice;
                    indice_vecino = OrdenVecino(vecino, vertice, G);

                    // Asignar color al vecino
                    FijarColor(color_vecino, indice_vecino, G);
                    // Colocar el vertice en la cola
                    Encolar(&q, indice_vecino);
                }else if(color_vecino == color_vertice){
                    // No es bipartito
                    es_bipartito = (char) 0;
                    break;
                }
            }
        }
    }

    // bool vertices_visitados[n];
    // memset(vertices_visitados, false, n*sizeof(bool));
    // Encolar(&q, 0);
    // FijarColor(0, 0, G);
    // while(!Vacia(&q) && es_bipartito == (char) 1){
    //     vertice = Decolar(&q);
    //     grado_vertice = Grado(vertice, G);
    //     color_vertice = Color(vertice, G);

    //     // Marcar como visitado
    //     vertices_visitados[vertice] = true;

    //     for(u32 vecino = 0; vecino < grado_vertice; vecino++){
    //         color_vecino = ColorVecino(vecino, vertice, G);
    //         if(color_vecino == 0xffffffff){
    //             color_vecino = 1 - color_vertice;
    //             indice_vecino = OrdenVecino(vecino, vertice, G);

    //             // Asignar color al vecino
    //             FijarColor(color_vecino, indice_vecino, G);
    //             // Colocar el vertice en la cola
    //             Encolar(&q, indice_vecino);
    //         }else if(color_vecino == color_vertice){
    //             // No es bipartito
    //             es_bipartito = (char) 0;
    //         }
    //     }

    //     if(Vacia(&q)){
    //         for(u32 i = 0; i < n; i++){
    //             if(vertices_visitados[i] == false){
    //                 // No forma parte de una componente conexa, lo coloreo y agrego a la cola
    //                 FijarColor(0, i, G);
    //                 Encolar(&q, i);
    //                 break;
    //             }
    //         }
    //     }
    // }

    if(!es_bipartito)
        Greedy(G);

    DestruirCola(&q);
    return es_bipartito;
}

/*SwitchColores Verifica que i,j <número de colores que 
tiene G en ese momento. Si no es cierto, retorna 1.
 Si ambos estan en el intervalo permitido, entonces 
 intercambia los colores i, j: Todos los vertices que esten
 coloreados en el coloreo actual con i pasan a tener 
 el color j y los que estan coloreados con j pasan a tener 
 el color i. Retorna 0 si todo se hizo bien.*/
char SwitchColores(Grafo G,u32 i,u32 j){
    u32 cant_colores = 0;
    u32 n = NumeroDeVertices(G);
    // u32 temp = 0xffffffff;

    for (u32 u = 0; u < n; u++)
     cant_colores = MAX(cant_colores, Color(u,G)); 

    cant_colores = cant_colores + 1; 
    printf("cant_colores:%u \n", cant_colores);

    if(i < cant_colores && j < cant_colores){
        //todos los que tienen color j, les doy el color -1.
        //todos los que tienen el color i, les doy el color j.
        for(u32 iter = 0; iter < n; iter++){
            if(Color(iter,G) == j){
                FijarColor(i, iter, G);
            }else if(Color(iter,G) == i){
                FijarColor(j, iter, G);
            }
        }
        // //todos los que tienen el color -1, les doy el color i.
        // for(u32 iter = 0; iter < n; iter++){
        //     if(Color(iter,G) == temp)
        //         FijarColor(i,iter,G);
        // }
        return 0;
    }else{
        //si i,j > n:
        return 1;
    }
}