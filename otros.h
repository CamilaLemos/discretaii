#ifndef __OTROS_H
#define __OTROS_H

#include <stdio.h>
#include <stdlib.h>



typedef struct Dupla{u32 v1;u32 v2;} Dupla;

typedef struct Trupla{u32 v1;u32 v2;u32 v3;}Trupla;

#define MAX(x, y) (((x) > (y)) ? (x) : (y))

/*Estructura Cola con sus funciones
  de encolar,decolar, si está vacía,
  crear y destruir la cola. La usamos para
  hacer bfs.*/
typedef struct Cola{
    u32 cabeza;
    u32 cola;
    u32 *fila;
}Cola;

//cola.c
Cola CrearCola(u32 size);
void DestruirCola(struct Cola *q);
void Encolar(struct Cola *q,u32 nodo);
u32 Decolar(struct Cola *q);
u32 Vacia(struct Cola *q);

//aux.c
int compareV2(const void *a, const void *b);
int compareColorCG(const void *a, const void *b);
int compareCGBC(const void *a, const void *b);
char frecuenciaColores(Trupla *b, u32 n);
void OrdenNat(Grafo G);
void swap(u32* a, u32* b);

#endif